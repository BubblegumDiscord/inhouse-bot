﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;

using System.IO;
using Microsoft.Extensions.Configuration;
using BubblegumBot.Core;


namespace BubblegumBot
{
    class Program
    {
        private DiscordSocketClient Client;
        private CommandService Commands;

        public static IConfiguration Configuration { get; set; }

        public static RiotSharp.RiotApi RiotAPI;

        public static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        public async Task MainAsync()
        {
            // locate
            try
            {
                var builder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddXmlFile("config/AppSettings.config");

                Configuration = builder.Build();
            }
            catch (Exception ex)
            {
                Console.WriteLine("XML not found, Error: " + ex.Message);
            }

            JWTIntegration.eco_base_uri = Configuration["eco_base_url"];
            JWTIntegration.getbal_uri = Configuration["eco_getbal"];
            JWTIntegration.setbal_uri = Configuration["eco_setbal"];
            JWTIntegration.key = Configuration["eco_key"];
            Console.Write("Booted");
            Client = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Error,
            });

            Commands = new CommandService(new CommandServiceConfig
            {
                CaseSensitiveCommands = false,
                DefaultRunMode = RunMode.Sync,
                LogLevel = LogSeverity.Debug
            });

            Client.MessageReceived += Client_MessageReceived;
            await Commands.AddModulesAsync(Assembly.GetEntryAssembly());

            Client.Ready += Client_Ready;
            Client.Log += Client_Log;

            RiotAPI = RiotSharp.RiotApi.GetInstance(Configuration["riot_api_key"], 200, 500);

            string token = Configuration["bot_token"];
            await Client.LoginAsync(TokenType.Bot, token);
            await Client.StartAsync();

            // make run forever
            await Task.Delay(-1);
        }

        private async Task Client_Log(LogMessage arg)
        {
            Console.WriteLine($"{DateTime.Now} at {arg.Source}] {arg.Message}");
        }

        private async Task Client_Ready()
        {
            Console.Write("Ready!");
            //await Client.SetGameAsync("Watching anime");
            await Client.SetGameAsync("Death is just the beginning...");
        }

        private async Task Client_MessageReceived(SocketMessage MessageParam)
        {
            var Message = MessageParam as SocketUserMessage;
            var Context = new SocketCommandContext(Client, Message);

            if (Context.Message.Content == null || Context.Message.Content == "") return;

            int ArgPos = 0;
            if (!(Context.Message.HasStringPrefix(".", ref ArgPos) || Message.HasMentionPrefix(Client.CurrentUser, ref ArgPos))) return;

            var Result = await Commands.ExecuteAsync(Context, ArgPos);
            if( !Result.IsSuccess )
            {
                Console.WriteLine($"{DateTime.Now} at Commands] Something went wrong with executing a command. Text: {Message.Content}, Error: {Result.ErrorReason}");
            }
        }
    }
}
