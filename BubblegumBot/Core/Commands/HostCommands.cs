﻿using Discord.Commands;
using Discord.Rest;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RiotSharp;
using RiotSharp.Endpoints.SummonerEndpoint;
using Discord;
using System.Linq;
using RiotSharp.Endpoints.LeagueEndpoint;

namespace BubblegumBot.Core.Commands
{
    public class HostCommands : ModuleBase<SocketCommandContext>
    {
        public static List<Lobby> Lobbies = new List<Lobby>();
        public static int IncrementalId = 0;
        public static Lobby ActiveLobby = null;

        [Command("host")]
        public async Task DoHost(params string[] NameArr)
        {
            if( !isInhouseMod() )
            {
                // not an inhouse mod
                await Context.Message.DeleteAsync();
                return;
            }

            // CHECKS
            if (ActiveLobby != null)
            {
                await EmbedResponse("Cannot host, Active Lobby already in progress.");
                return;
            }

            string name = "";
            RiotSharp.Misc.Region region;

            bool regionNotSpecified = false;
            if (NameArr == null || NameArr.Length == 0)
            {
                //await EmbedResponse("No region specified, defaulting to NA.");
                region = RiotSharp.Misc.Region.na;
            }
            else
            {
                switch (NameArr[0].ToLower())
                {
                    case "na":
                        region = RiotSharp.Misc.Region.na;
                        break;
                    case "euw":
                        region = RiotSharp.Misc.Region.euw;
                        break;
                    case "eune":
                        region = RiotSharp.Misc.Region.eune;
                        break;
                    default:
                        //await EmbedResponse("No region specified, defaulting to NA.");
                        region = RiotSharp.Misc.Region.na;
                        regionNotSpecified = true;
                        break;
                }
                for (int i = 0; i < NameArr.Length; i++)
                {
                    if (!regionNotSpecified && i == 0) continue;
                    name += NameArr[i];
                    if (i < NameArr.Length - 1) name += " ";
                }
            }

            // if name is nothing, then dont join own lobby
            Player player = null;
            Summoner summoner = null;
            if (!name.Equals("", StringComparison.CurrentCultureIgnoreCase))
            {
                try
                {
                    summoner = await Program.RiotAPI.Summoner.GetSummonerByNameAsync(region, name);
                    //await EmbedResponse("Joining lobby as summoner: " + summoner.Name);
                }
                catch (Exception ex)
                {
                    //await EmbedResponse("Failed to find summoner: " + name);
                }
            }
            if (summoner != null)
            {
                player = new Player(Context.User as SocketGuildUser, summoner);

                // calculate rank value
                player.RankValue = await CalcSummonerRankValue(region, player.Summoner, player);
                //await EmbedResponse("Rank Value: " + player.RankValue);
            }

            // create the lobby
            var lobby = new Lobby(Context.User as SocketGuildUser, region);
            lobby.Host = Context.User as SocketGuildUser;

            // join the lobby?
            if (player != null)
                lobby.Players.Add(player);

            // set the active lobby
            ActiveLobby = lobby;

            // display the lobby
            await DisplayActiveLobby();
        }

        [Command("draft")]
        public async Task DoDraft()
        {
            if (!isInhouseMod())
            {
                // not an inhouse mod
                await Context.Message.DeleteAsync();
                return;
            }

            // checks
            if (ActiveLobby == null)
            {
                await EmbedResponse("Cannot draft. No Active Lobby.");
                return;
            }

            if (ActiveLobby.Players.Count() < ActiveLobby.MaxPlayers)
            {
                await EmbedResponse("Cannot draft. Lobby not full.");
                return;
            }

            // do draft POST CHECKS
            ActiveLobby.Players.Sort((x, y) => x.RankValue.CompareTo(y.RankValue));
            ActiveLobby.Players.Reverse();

            // assign to teams
            for (int i = 0; i < ActiveLobby.Players.Count(); i++)
            {
                if (i % 2 == 0)
                    ActiveLobby.RedPlayers.Add(ActiveLobby.Players[i]);
                else
                    ActiveLobby.BluePlayers.Add(ActiveLobby.Players[i]);
            }

            // assign player index
            for( int i = 0; i < ActiveLobby.RedPlayers.Count(); i++ )
            {
                ActiveLobby.RedPlayers.ElementAt(i).teamIndex = i + 1;
            }
            for (int i = 0; i < ActiveLobby.BluePlayers.Count(); i++)
            {
                ActiveLobby.BluePlayers.ElementAt(i).teamIndex = i + 1;
            }

            // sort it
            ActiveLobby.BluePlayers.Sort((x, y) => x.teamIndex.CompareTo(y.teamIndex));
            ActiveLobby.RedPlayers.Sort((x, y) => x.teamIndex.CompareTo(y.teamIndex));

            // now delete players list
            ActiveLobby.Players.Clear();

            // set to drafting
            ActiveLobby.isDrafted = true;

            // id
            IncrementalId++;
            ActiveLobby.Id = IncrementalId;

            // timestamp
            ActiveLobby.timeStamp = DateTime.Now;

            // add to lobbies list
            Lobbies.Add(ActiveLobby);
            ActiveLobby = null;

            await DisplayDraftedLobby(IncrementalId);
        }

        [Command("ign")]
        public async Task DoIgn(params string[] nameArr)
        {
            string name = "";

            // check if name was specified
            if (nameArr == null || nameArr.Length == 0)
            {
                await EmbedResponse("Specify IGN.");
                return;
            }

            // check if lobby exists
            if (ActiveLobby == null)
            {
                await EmbedResponse("No active In-House.");
                return;
            }

            // is the lobby drafted?
            if (ActiveLobby.isDrafted)
            {
                await EmbedResponse("Cannot join. Lobby is already drafted.");
                return;
            }

            // create name
            for (int i = 0; i < nameArr.Length; i++)
            {
                name += nameArr[i];
                if (i < nameArr.Length - 1) name += " ";
            }

            // duplicate player check
            bool duplicatePlayerCheck = true;
            if (ActiveLobby.Players.Where(x => x.User.Id == Context.User.Id).Count() > 0 && duplicatePlayerCheck)
            {
                ActiveLobby.Players.Remove(ActiveLobby.Players.Where(x => x.User.Id == Context.User.Id).FirstOrDefault());
            }
            else if (ActiveLobby.Players.Count() >= ActiveLobby.MaxPlayers)
            {
                await Context.Message.DeleteAsync();
                return;
            }

            // POST CHECKS ~~~~~~~~~~~~~~~~

            // try to find summoner
            // if name is nothing, then dont join own lobby
            Player player = null;
            Summoner summoner = null;
            if (!name.Equals("", StringComparison.CurrentCultureIgnoreCase))
            {
                try
                {
                    summoner = await Program.RiotAPI.Summoner.GetSummonerByNameAsync(ActiveLobby.Region, name);
                }
                catch (Exception ex)
                {
                    await EmbedResponse("Failed to find summoner: " + name);
                    return;
                }
            }
            if (summoner != null)
            {
                // duplicate player CHECK
                if( ActiveLobby.Players.Where(x=>x.Summoner.Name.Equals(summoner.Name, StringComparison.CurrentCultureIgnoreCase)).Count() > 0 && duplicatePlayerCheck )
                {
                    await EmbedResponse($"Someone already in lobby with name {summoner.Name}");
                    return;
                }

                player = new Player(Context.User as SocketGuildUser, summoner);

                // calculate rank value
                player.RankValue = await CalcSummonerRankValue(ActiveLobby.Region, player.Summoner, player);
            }

            // add the player to the lobby
            ActiveLobby.Players.Add(player);

            // display the lobby
            await DisplayActiveLobby();
            await Context.Message.DeleteAsync();
        }

        [Command("drop")]
        public async Task DoDrop()
        {
            // checks
            // check if lobby exists
            if (ActiveLobby == null)
            {
                await EmbedResponse("No active In-House.");
                return;
            }

            // is the lobby drafted?
            if (ActiveLobby.isDrafted)
            {
                await EmbedResponse("Cannot join. Lobby is already drafted.");
                return;
            }

            // check if user is in lobby
            if (ActiveLobby.Players.Where(x => x.User.Id == Context.User.Id).Count() < 1)
            {
                await EmbedResponse($"User <@{Context.User.Id}> not found.");
                return;
            }

            // POST CHECKS

            // get the player
            var player = ActiveLobby.Players.Where(x => x.User.Id == Context.User.Id).FirstOrDefault();

            // drop the dro
            ActiveLobby.Players.Remove(player);

            // display lobby
            await DisplayActiveLobby();

            // delete mess
            await Context.Message.DeleteAsync();

        }

        /*
        [Command("test$")]
        public async Task DoTestMon(ulong id)
        {
            int bal = await JWTIntegration.GetBalance(id);
            await EmbedResponse("Bal: " + bal);
        }

        [Command("testset$")]
        public async Task DoSetMoney(ulong id, long money)
        {
            await JWTIntegration.SetBalance(id, money);
            await EmbedResponse("done");
        }
        */

        [Command("lobby")]
        public async Task DoLobby(int id = -1)
        {
            if (id == -1)
            {
                // attempt to display non drafted lobby
                if (ActiveLobby != null)
                {
                    await DisplayActiveLobby();
                    return;
                }
                else
                {
                    await EmbedResponse("No active lobby. Try `.lobbies` to view drafted lobbies.");
                    return;
                }
            }

            if (Lobbies.Where(x => x.Id == id).Count() < 1)
            {
                await EmbedResponse($"Lobby with Id: `{id}` not found.");
                return;
            }

            // display the lobby, it exists
            await DisplayDraftedLobby(id);
        }

        [Command("swap")]
        public async Task DoSwap( int id = -1, int blueIndex = -1, int redIndex = -1 )
        {
            if( !isInhouseMod() )
            {
                // not an inhouse mod, just delete it
                await Context.Message.DeleteAsync();
                return;
            }

            if( id == -1 || blueIndex == -1 || redIndex == -1 )
            {
                // id not specified
                await EmbedResponse("Please specify lobby id. Ex command usage. `.swap 2 1 1`. Swaps player 1 from blue team with player 1 from red team. In lobby with id `2`.");
                return;
            }

            // greater than 5
            if( blueIndex > 5 || redIndex > 5 )
            {
                await EmbedResponse("Please specify lobby id. Ex command usage. `.swap 2 1 1`. Swaps player 1 from blue team with player 1 from red team. In lobby with id `2`.");
                return;
            }

            // check if lobby exists
            if( Lobbies.Where(x=>x.Id == id).Count() < 1 )
            {
                await EmbedResponse($"No lobby found with Id: `{id}`");
                return;
            }

            // do swap POST CHECKS
            var lobby = Lobbies.Where(x => x.Id == id).FirstOrDefault();

            var bluePlayer = lobby.BluePlayers.Where(x=>x.teamIndex == blueIndex ).FirstOrDefault();
            var redPlayer = lobby.RedPlayers.Where(x=>x.teamIndex == redIndex).FirstOrDefault();
            lobby.BluePlayers[lobby.BluePlayers.IndexOf(bluePlayer)] = redPlayer;
            lobby.RedPlayers[lobby.RedPlayers.IndexOf(redPlayer)] = bluePlayer;

            // sort by rank
            lobby.BluePlayers.Sort((x, y) => x.RankValue.CompareTo(y.RankValue));
            lobby.BluePlayers.Reverse();
            lobby.RedPlayers.Sort((x, y) => x.RankValue.CompareTo(y.RankValue));
            lobby.RedPlayers.Reverse();

            // assign player index
            for (int i = 0; i < lobby.RedPlayers.Count(); i++)
            {
                lobby.RedPlayers.ElementAt(i).teamIndex = i + 1;
            }
            for (int i = 0; i < lobby.BluePlayers.Count(); i++)
            {
                lobby.BluePlayers.ElementAt(i).teamIndex = i + 1;
            }

            // sort it
            lobby.BluePlayers.Sort((x, y) => x.teamIndex.CompareTo(y.teamIndex));
            lobby.RedPlayers.Sort((x, y) => x.teamIndex.CompareTo(y.teamIndex));

            Lobbies[Lobbies.IndexOf(lobby)] = lobby;

            // display it
            await EmbedResponse($"Swapped `{bluePlayer.Summoner.Name}` with `{redPlayer.Summoner.Name}`");
            await DisplayDraftedLobby(id);
        }

        [Command("lobbies")]
        public async Task DoLobbies()
        {
            // display list of all lobbyes
            var embed = new EmbedBuilder();
            embed.WithTitle("In-House Lobbies");
            if (ActiveLobby == null && Lobbies.Count() < 1)
            {
                await EmbedResponse("No lobbies found.");
                return;
            }
            if (ActiveLobby != null)
            {
                embed.AddInlineField("__Active Lobby__", $"Host: {ActiveLobby.Host.Username}\n# Players: {ActiveLobby.Players.Count()}");
            }
            foreach (var lobby in Lobbies)
            {
                embed.AddInlineField($"__Drafted Lobby__ `#{lobby.Id}`", $"Host: {lobby.Host.Username}\nTime: {lobby.timeStamp.ToShortTimeString()} GMT   ");
            }

            await Context.Channel.SendMessageAsync("", false, embed);
        }

        [Command("clear")]
        public async Task DoClear(int lobbyId = -1 )
        {
            if (!isInhouseMod())
            {
                // not an inhouse mod
                await Context.Message.DeleteAsync();
                return;
            }

            if( lobbyId != -1 )
            {
                // clear that lobby
                if( Lobbies.Where(x=>x.Id == lobbyId).Count() < 1 )
                {
                    await EmbedResponse($"No lobby with Id `{lobbyId} found.`");
                    return;
                }
                // remove the lobby
                var lobb = Lobbies.Where(x => x.Id == lobbyId).FirstOrDefault();
                Lobbies.Remove(lobb);
                await EmbedResponse($":white_check_mark: Lobby with Id `{lobb.Id}` cleared.");
                return;
            }

            // check if there's an active lobby
            if( ActiveLobby == null )
            {
                await EmbedResponse("No active lobby.");
                return;
            }

            // clear the active lobby
            ActiveLobby = null;

            await EmbedResponse(":white_check_mark: Active lobby cleared.");
        }

        [Command("reopen")]
        public async Task DoReOpen( int lobbyId )
        {
            if (!isInhouseMod())
            {
                // not an inhouse mod
                await Context.Message.DeleteAsync();
                return;
            }

            // check if lobby with id exists
            if( Lobbies.Where(x=>x.Id == lobbyId).Count() < 1 )
            {
                await EmbedResponse($"No lobby with Id `{lobbyId}` found.");
                return;
            }

            // check if there's currently an active lobby
            if( ActiveLobby != null )
            {
                await EmbedResponse("Cannot reopen lobby, an active lobby currently exists. Clear it first.");
                return;
            }

            // POST CHECKS, reopen the lobby
            ActiveLobby = Lobbies.Where(x => x.Id == lobbyId).FirstOrDefault();
            Lobbies.Remove(Lobbies.Where(x=>x.Id == lobbyId).FirstOrDefault());

            // set some vars
            ActiveLobby.isDrafted = false;
            foreach( var player in ActiveLobby.BluePlayers )
            {
                ActiveLobby.Players.Add(player);
            }
            foreach( var player in ActiveLobby.RedPlayers )
            {
                ActiveLobby.Players.Add(player);
            }
            // clear teams
            ActiveLobby.BluePlayers.Clear();
            ActiveLobby.RedPlayers.Clear();

            // display lobby
            await EmbedResponse($":white_check_mark: Lobby `{lobbyId}` reopened!");
            await DisplayActiveLobby();

        }

        [Command("remove")]
        public async Task DoRemove(params string[] nameArr)
        {
            if (!isInhouseMod())
            {
                // not an inhouse mod
                await Context.Message.DeleteAsync();
                return;
            }

            if (nameArr == null || nameArr.Length == 0)
            {
                await EmbedResponse("Please specify Summoner Name to remove");
                return;
            }

            // create name
            string name = "";
            for (int i = 0; i < nameArr.Length; i++)
            {
                name += nameArr[i];
                if (i < nameArr.Length - 1)
                    name += " ";
            }

            // try to find user
            if (ActiveLobby.Players.Where(x => x.Summoner.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)).Count() < 1)
            {
                await EmbedResponse($"Cannot find user with name: `{name}`");
                return;
            }

            // remove the player
            ActiveLobby.Players.Remove(ActiveLobby.Players.Where(x => x.Summoner.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault());

            // display the lobby
            await DisplayActiveLobby();

            await Context.Message.DeleteAsync();
        }

        [Command("win")]
        public async Task DoWin(int lobbyId, string team)
        {
            if (!isInhouseMod())
            {
                // not an inhouse mod
                await Context.Message.DeleteAsync();
                return;
            }

            // check if lobby with specified id exists
            if (Lobbies.Where(x => x.Id == lobbyId).Count() < 1)
            {
                await EmbedResponse($"No lobby found with Id: `{lobbyId}`");
                return;
            }

            var lobby = Lobbies.Where(x => x.Id == lobbyId).FirstOrDefault();

            // POST CHECKS
            // assign the win
            await EmbedResponse("Awarding <a:ruru:408256569483067403>");

            List<Player> winningPlayers = new List<Player>();
            List<Player> losingPlayers = new List<Player>();
            string winningTeamColor = "";
            string losingTeamColor = "";
            switch (team.ToLower())
            {
                case "blue":
                    // assign money to blue team
                    winningPlayers = lobby.BluePlayers.ToList();
                    losingPlayers = lobby.RedPlayers.ToList();
                    winningTeamColor = "Blue";
                    losingTeamColor = "Red";
                    break;
                case "red":
                    // assign money to red team
                    winningPlayers = lobby.RedPlayers.ToList();
                    losingPlayers = lobby.BluePlayers.ToList();
                    winningTeamColor = "Red";
                    losingTeamColor = "Blue";
                    break;
                default:
                    await EmbedResponse($"Unrecognized team `{team}`. Please enter either `blue` or `red`. ");
                    return;
            }

            // give monies
            try
            {
                foreach (var player in winningPlayers)
                {
                    // assign money
                    await JWTIntegration.SetBalance(player.User.Id, (await JWTIntegration.GetBalance(player.User.Id)) + long.Parse(Program.Configuration["money_per_win"]));
                    //await Context.Channel.SendMessageAsync("Awarded `" + player.User.Username + "` " + Program.Configuration["money_per_win"]);
                    await Task.Delay(100);
                }
                foreach (var player in losingPlayers)
                {
                    // assign money
                    await JWTIntegration.SetBalance(player.User.Id, (await JWTIntegration.GetBalance(player.User.Id)) + long.Parse(Program.Configuration["money_per_loss"]));
                    //await Context.Channel.SendMessageAsync("Awarded `" + player.User.Username + "` " + Program.Configuration["money_per_loss"]);
                    await Task.Delay(100);
                }
            }
            catch( Exception ex )
            {
                // log error
                ulong channelId1 = 0;
                ulong.TryParse(Program.Configuration["inhouse_log_id"], out channelId1);
                var logChannel = Context.Guild.TextChannels.Where(x => x.Id == channelId1).FirstOrDefault();
                await logChannel.SendMessageAsync(ex.Message + " | " + ex.InnerException);
            }

            // now just create an image to display this
            // temporary display win until image is in place
            string players = "";
            for( int i = 0; i < winningPlayers.Count(); i++ )
            {
                string userName = winningPlayers[i].User.Username;
                if( userName.Length > 12 )
                {
                    userName = userName.Substring(0, 12);
                }
                string monies = int.Parse(Program.Configuration["money_per_win"]).ToString("N0");
                players += string.Format("{0,-17}{1,-16}{2,-16}", $"{userName}#{winningPlayers[i].User.Discriminator}", winningPlayers[i].Summoner.Name, monies) + Environment.NewLine;
            }
            
            var embed = new EmbedBuilder();
            string header = string.Format("{0,-17}{1,-16}{2,-16}", "DISCORD", "IGN", "AWARD" );
            embed.WithDescription($"```prolog\n{header}``````css\n{players}```");

            embed.WithAuthor($"Bubblegum In-House Winners! ", Context.Guild.IconUrl);
            embed.WithFooter($"Host: {lobby.Host.Username} ", lobby.Host.GetAvatarUrl());
            embed.WithCurrentTimestamp();

            if( winningTeamColor.ToLower().Equals("red") )
            {
                embed.WithColor(Color.Red);
            }
            else
            {
                embed.WithColor(Color.Blue);
            }

            await Context.Channel.SendMessageAsync("", false, embed);
            ulong channelId = 0;
            ulong.TryParse(Program.Configuration["inhouse_log_id"], out channelId);
            var logChannel2 = Context.Guild.TextChannels.Where(x => x.Id == channelId).FirstOrDefault();
            await logChannel2.SendMessageAsync($"Win put in by: {Context.User.Username}#{Context.User.Discriminator} - uid: `{Context.User.Id}`", false, embed);

            // done so delete lobby
            Lobbies.Remove(lobby);
        }

        public async Task DisplayActiveLobby()
        {
            var embed = new EmbedBuilder();

            // title
            embed.WithAuthor($"Bubblegum In-House Lobby [{ActiveLobby.Region.ToString().ToUpper()}]", Context.Guild.IconUrl);

            // host
            embed.WithFooter($"Host: " + ActiveLobby.Host.Username, ActiveLobby.Host.GetAvatarUrl());

            // image
            embed.WithThumbnailUrl("https://vignette.wikia.nocookie.net/finalfantasy/images/3/3f/Cloud_VS_Sephiroth.png/revision/latest?cb=20090715134430");

            // get the players
            string players = "Join the lobby with \n.ign Summoner Name";
            for (int i = 0; i < ActiveLobby.Players.Count; i++)
            {
                if (i == 0) players = "";
                if (i < 9)
                    players += string.Format("{0,-3}{1,-16}{2,5}", i + 1 + ". ", ActiveLobby.Players[i].Summoner.Name, "[" + ActiveLobby.Players[i].Rank + "]\n");
                else
                    players += string.Format("{0,-3}{1,-16}{2,5}", i + 1 + ".", ActiveLobby.Players[i].Summoner.Name, "[" + ActiveLobby.Players[i].Rank + "]\n");
            }
            players = "```" + players + "```";
            embed.WithDescription(players);

            // delete prev ones
            List<RestUserMessage> MsgsToDelete = new List<RestUserMessage>();
            foreach (var mess in ActiveLobby.PreDraftLobbyMsgs)
            {
                try
                {
                    if (mess != null)
                    {
                        await mess.DeleteAsync();
                        MsgsToDelete.Add(mess);
                    }
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
            foreach (var mess in MsgsToDelete)
            {
                ActiveLobby.PreDraftLobbyMsgs.Remove(mess);
            }

            var messToSend = await Context.Channel.SendMessageAsync("", false, embed);

            // add to list
            ActiveLobby.PreDraftLobbyMsgs.Add(messToSend);
        }
        public async Task DisplayDraftedLobby(int lobbyId)
        {
            var embed = new EmbedBuilder();
            var lobby = Lobbies.Where(x => x.Id == lobbyId).FirstOrDefault();

            // title
            embed.WithAuthor($"Bubblegum In-House Lobby [{lobby.Region.ToString().ToUpper()}]", Context.Guild.IconUrl);

            // host
            embed.WithFooter($"Host: " + lobby.Host.Username, lobby.Host.GetAvatarUrl());

            // image
            embed.WithThumbnailUrl("https://stmed.net/sites/default/files/styles/1920x1080/public/anime-wallpapers-25720-5118456.jpg?itok=Dp5JHV05");

            // DO CONTEXT HERE
            string t1Players = "";
            string t2Players = "";

            for (int i = 0; i < lobby.BluePlayers.Count(); i++)
            {
                t1Players += $"{lobby.BluePlayers[i].teamIndex}. {lobby.BluePlayers[i].Summoner.Name} [{lobby.BluePlayers[i].Rank}]\n";
            }
            for (int i = 0; i < lobby.RedPlayers.Count(); i++)
            {
                t2Players += $"{lobby.RedPlayers[i].teamIndex}. {lobby.RedPlayers[i].Summoner.Name} [{lobby.RedPlayers[i].Rank}]\n";
            }

            // add inline thingies
            embed.AddField("__Blue Team:__ ", "```" + t1Players + "```");
            embed.AddField("__Red Team:__", "```" + t2Players + "```");

            // END CONTEXT HERE

            // delete prev ones
            List<RestUserMessage> MsgsToDelete = new List<RestUserMessage>();
            foreach (var mess in lobby.PreDraftLobbyMsgs)
            {
                try
                {
                    if (mess != null)
                    {
                        await mess.DeleteAsync();
                        MsgsToDelete.Add(mess);
                    }
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
            foreach (var mess in MsgsToDelete)
            {
                lobby.PreDraftLobbyMsgs.Remove(mess);
            }

            var messToSend = await Context.Channel.SendMessageAsync("", false, embed);

            // add to list
            lobby.PreDraftLobbyMsgs.Add(messToSend);
        }

        public async Task EmbedResponse(String Message, bool deleteContextMess = false)
        {
            var embed = new EmbedBuilder();
            embed.WithDescription(Message);
            await Context.Channel.SendMessageAsync("", false, embed);

            if (deleteContextMess)
                await Context.Message.DeleteAsync();
        }

        public async Task<int> CalcSummonerRankValue(RiotSharp.Misc.Region region, Summoner summoner, Player player)
        {
            LeaguePosition league = null;
            try
            {
                var leaguePosses = await Program.RiotAPI.League.GetLeaguePositionsAsync(region, summoner.Id);
                league = leaguePosses.Where(x => x.QueueType == RiotSharp.Misc.Queue.RankedSolo5x5).FirstOrDefault();
                if (league.Tier.Length > 0)
                    player.Rank = (league.Tier.ElementAt(0) + "").ToUpper(); ;
                if (league.Rank.Length >= 0 && player.Rank != "C" && player.Rank != "M")
                {
                    if (league.Rank.Equals("V")) player.Rank += 5;
                    else if (league.Rank.Equals("IV")) player.Rank += 4;
                    else if (league.Rank.Equals("III")) player.Rank += 3;
                    else if (league.Rank.Equals("II")) player.Rank += 2;
                    else if (league.Rank.Equals("I")) player.Rank += 1;
                }
                //await EmbedResponse($"League found: [{player.Rank}]");
            }
            catch (Exception ex)
            {
                //await EmbedResponse("Failed to find rank, defaulting to UNRANKED");
                player.Rank = "UR";
                return 0;
            }

            // calc rank now
            int result = 0;

            if (league.Tier.ToLower().Contains("chal")) result += 100;
            else if (league.Tier.ToLower().Contains("mas")) result += 90;
            else if (league.Tier.ToLower().Contains("dia")) result += 80;
            else if (league.Tier.ToLower().Contains("plat")) result += 70;
            else if (league.Tier.ToLower().Contains("gold")) result += 60;
            else if (league.Tier.ToLower().Contains("silv")) result += 50;
            else if (league.Tier.ToLower().Contains("bronz")) result += 40;

            if (league.Rank.Equals("V")) result += 1;
            else if (league.Rank.Equals("IV")) result += 2;
            else if (league.Rank.Equals("III")) result += 3;
            else if (league.Rank.Equals("II")) result += 4;
            else if (league.Rank.Equals("I")) result += 5;

            return result;
        }

        public bool isInhouseMod()
        {
            if( (Context.User as SocketGuildUser).Roles.Where(x=>x.Name.Equals(Program.Configuration["host_role"], StringComparison.CurrentCultureIgnoreCase)).Count() < 1 )
            {
                // user is not inhouse mods
                return false;
            }
            return true;
        }

        // kill the bot
        [RequireUserPermission( GuildPermission.Administrator)]
        [Command("die")]
        public async Task DoDie()
        {
            double msDelay = 1500;
            var mess = await Context.Channel.SendMessageAsync($"Killing Bot in {msDelay / 1000} seconds");

            // delete verify message if it exists
            //if (Verify.reactRoleMess != null) await Verify.reactRoleMess.DeleteAsync();

            await Task.Delay((int)msDelay);
            await mess.DeleteAsync();
            await Context.Message.DeleteAsync();
            Environment.Exit(-1);
        }
    }

    public class Lobby
    {
        // list of players
        public List<Player> Players = new List<Player>();

        // lobby id
        public int Id;

        // max players
        public int MaxPlayers = 10;

        // teams
        public List<Player> BluePlayers = new List<Player>();
        public List<Player> RedPlayers = new List<Player>();

        // host user
        public SocketGuildUser Host;

        // region
        public RiotSharp.Misc.Region Region;

        // is drafted?
        public bool isDrafted = false;

        // timestamp?
        public DateTime timeStamp;

        public Lobby( SocketGuildUser Host, RiotSharp.Misc.Region Region )
        {
            this.Host = Host;
            this.Region = Region;
        }

        // msgs for lobby
        public List<RestUserMessage> PreDraftLobbyMsgs = new List<RestUserMessage>();
    }

    public class Player
    {
        public SocketGuildUser User;
        public Summoner Summoner;
        public int RankValue = -1;
        public string Rank = "";

        public int teamIndex;

        public Player( SocketGuildUser User, Summoner Summoner )
        {
            this.User = User;
            this.Summoner = Summoner;
        }
    }
}
