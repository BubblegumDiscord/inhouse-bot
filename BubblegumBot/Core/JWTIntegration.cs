﻿using JWT;
using JWT.Algorithms;
using JWT.Builder;
using JWT.Serializers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BubblegumBot.Core
{
    public class JWTIntegration
    {
        public static string eco_base_uri;
        public static string setbal_uri;
        public static string getbal_uri;
        public static string key;

        public static async Task<int> GetBalance( ulong id )
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            long msecondsSinceEpoch = (long)t.TotalMilliseconds;

            var payload = new Dictionary<string, object>
            {
                { "uid", id.ToString() },
                { "timeIssued", msecondsSinceEpoch }
            };

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = encoder.Encode(payload, key);

            WebRequest req = WebRequest.Create(eco_base_uri + getbal_uri + token);
            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        // Put in code to check response status. You'll probably get a 404 when not found.
                        try
                        {
                            GetBalance bal = (GetBalance)serializer.Deserialize<GetBalance>(readStream.ReadToEnd());
                            return bal.balance;
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine("failed");
                            return -1;
                        }
                    }
                }
            }
        }

        public static async Task SetBalance(ulong id, long money)
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            long msecondsSinceEpoch = (long)t.TotalMilliseconds;

            var payload = new Dictionary<string, object>
            {
                { "uid", id.ToString() },
                { "amount", money },
                { "timeIssued", msecondsSinceEpoch }
            };

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = encoder.Encode(payload, key);

            // Create a request for the URL
            WebRequest req = WebRequest.Create(eco_base_uri + setbal_uri + token);
            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        //Console.WriteLine(readStream.ReadToEnd());
                    }
                }
            }
        }
    }
}

class GetBalance
{
    public int balance { get; set; }
}
