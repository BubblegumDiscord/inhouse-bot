from microsoft/dotnet:2.1-sdk as build
workdir /build
add . .
run dotnet publish

from microsoft/dotnet:2.1-sdk
workdir /bot
copy --from=build /build/BubblegumBot/bin/Debug/netcoreapp2.0/publish /bot
cmd dotnet BubblegumBot.dll